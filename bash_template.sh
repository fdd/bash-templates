#!/bin/bash
# bash_template.sh: template for robust bash scripts.
#
# ijula | Jun 28, 2018 A.D. | mailto: ionut.jula@emerson.com
# /* This is free and unencumbered software released into the public domain. */

set -o nounset    # treat unset variables as an error when substituting.
set -o pipefail   # consider errors inside a pipe as pipeline errors.
shopt -s extglob  # extended pattern matching.

## global error defs.
declare -ri SUCCESS='0'
declare -ri E_NO_ROOT='100'
declare -ri E_NO_ARGS='101'
declare -ri E_NO_ACCESS='102'
declare -ri E_INVAL_OPT='103'
declare -ri E_INVAL_OS='104'
declare -ri E_INVAL_BASH='105'
declare -ri E_NO_PREREQ='106'
declare -ri E_NO_VM='107'
declare -ri E_NO_METAL='108'
declare -ri E_NO_LOCK='110'
declare -ri E_CLEANUP='111'

## global definitions.
# ANSI escape sequences for some colors.
declare -r c_red="\\033[01;31m"
declare -r c_yellow="\\033[01;33m"
declare -r c_green="\\033[01;32m"
declare -r c_off="\\033[0m"
# static global variables.
# dynamic global variables.


# main: magic starts here.
main()
{
    local -i return_code     # global return code.
    local opt                # getopts options container.
    local opt_show_help='0'  # show_help() flag.
    local opt_function_f='0' # function_f() flag.
    local script_basename    # basename of the script file.

    return_code='0' # init to avoid unbond errors.
    #script_basename="$(basename -- "$0")"   # basename of the script file.
    script_basename="${0##*/}"               # using POSIX substitution.

    # set the time format for the bash time builtin (now w/ colors!).
    # prints only the real elapsed time, in a long format, 3-digit precision.
    TIMEFORMAT=$"$(echo -e "${c_yellow}")[stderr]$(echo -e "${c_off}") Execution time: %3lR"

    while getopts ':hf' opt; do     # prepend the option-string w/ ':'.
        case "$opt" in              # getopts is in silent mode.
            h)
                #echo '-h option selected.'' >&2  # _DEBUG.
                opt_show_help='1'
                ;;
            f)
                #echo '-f option selected.'' >&2  # _DEBUG.
                opt_function_f='1'
                ;;
            \?)
                echo_err "${script_basename}: invalid option -- '$OPTARG'"
                echo_err "Try \`${script_basename} -h' for more information."
                exit "$E_INVAL_OPT"
                ;;
        esac
    done


    if [[ "$opt_show_help" = '1' ]]; then
        show_help
    fi

    # correct order of the checks, if applicable:
    # 1. os version.
    # 2. hardware platform.
    # 3. bash version.
    # 4. user/EUID.

    # am_i_root

    if [[ "$opt_function_f" = '1' ]]; then
        fn1
        fn2
    fi

    return_code="$?"
    printf '\nreturn code: %s\n' "$return_code" # _DEBUG.

    return "$return_code"
}


# show_help: prints help and usage info.
show_help()
{
    declare script_basename    # basename of the script file.
    script_basename="${0##*/}" # using POSIX substitution.

    echo "Usage: ${script_basename} [OPTION]..."
    echo ''
    echo 'Description.'
    echo ''
    echo '  -h  print this help message and exit.'
    echo ''
    echo 'Send bug reports to: <ionut.jula@emerson.com>.'
    echo 'This is free and unencumbered software released into the public domain.'

    exit "$SUCCESS"
}

# printf_err: printf to stderr.
printf_err()
{
    printf '%s\n' "$@" 1>&2
}

# echo_err: echoes the supplied arguments to stderr.
echo_err()
{
    echo -e "$@" 1>&2
}

# am_i_root: to root or not to root?
am_i_root()
{
    if [[ "$EUID" -ne 0 ]]; then
        echo_err 'This must be run as root.'
        exit "$E_NO_ROOT"
    fi
}

# fn1: descr.
#      if any global variable is to be altered,
#      then this function must NOT be ran from a subshell.
fn1()
{
    local target_dir

    target_dir='/tmp/'

    # change dir done right.
    cd "$target_dir" || {
        echo_err "Cannot access ${target_dir}. ${c_red}Exiting.${c_off}"
        exit "$E_NO_ACCESS"
    }

    echo "this is function: ${FUNCNAME[0]}()."
    echo_err "this is an error from ${FUNCNAME[0]}()."
}

# fn2: descr.
# call: fn2 "$arg1"
fn2()
{
    # function arg check:
    if [[ $# -lt 1 ]]; then
        echo_err "${FUNCNAME[0]}(): no argument supplied, nothing to do."
        echo_err "usage: ${FUNCNAME[0]} <arg1> <arg1> ..."
        return "$E_NO_ARGS"
    fi

    # handle signals INT, TERM, and QUIT, then exit afterwards.
    #trap "echo; echo ${FUNCNAME[0]}\(\) interrupted.; exit" INT
    sighandler()
    {
        declare signal

        # function arg check:
        if [[ $# -lt 1 ]]; then
            echo_err "${FUNCNAME[0]}(): no argument supplied, nothing to do."
            echo_err "usage: ${FUNCNAME[0]} <input_signal>"
            return "$E_NO_ARGS"
        fi

        signal="$1"

        echo ''
        echo -en "${c_yellow}[ ${signal} ]${c_off} "
        echo -en "Execution interrupted (received ${signal})."
        # do cleanup here.

        exit "$SUCCESS"
    }

    trap 'sighandler SIGINT' INT
    trap 'sighandler SIGTERM' TERM
    trap 'sighandler SIGQUIT' QUIT

    echo -n "$c_yellow""$c_green"
    echo "this is function: ${FUNCNAME[0]}()."
    echo -n "$c_off"
    echo_err "this is an error from ${FUNCNAME[0]}()."
}

# round: evenly round an integer division operation using bash arithmetic.
round()
{
    # prime the numerator w/ "y/2".
    # instead of "x/y", calculate "(x+y/2)/y".
    echo $((($1 + $2/2) / $2))
}

# parallel: run a for loop in parallel.
parallel()
{
    declare -i i
    declare -a array=()

    # put every ssh job in the background, so we don't have to wait for it to finish.
    # the slowest job will determine the execution time of the entire loop.
    for i in "${!array[@]}"; do
        ( (echo -e "${c_yellow}Dev_${i}${c_off}"
            return_device_info "${array[i]}"
            echo "") | sed n # buffered output (all text at once).
        ) | sed n &          # buffer together all compound outputs.
    done
    wait # wait for all the jobs to finish before going further.
    echo_err -n "\\n${c_yellow}[stderr]${c_off} All subshells finished. "
    echo_err "${c_green}[ OK ]${c_off}" # _DEBUG.
}

# ssh_parallel: run multiple ssh connections in parallel.
ssh_parallel()
{
    declare server      # loop iterator var.
    declare server_list # server list.
    declare ssh_options # options to pass to ssh(1).

    ssh_options='-opt1 -opt2'
    server_list='serv1 serv2'

    # stderr is redirected to stdout not to lose any interesting error messages.
    # the sed trick appends the server name to each output line, thus identifying it.
    for server in $server_list; do
        (ssh $ssh_options -- "$ssh_user"@"$server" "$ssh_cmd 2>&1" 2>&1 \
            | sed -e "s/^/$server: /"
        ) &
    done
    wait # wait for all the jobs to finish before going further.
    echo_err -n "\\n${c_yellow}[stderr]${c_off} All ssh subshells finished. "
    echo_err "${c_green}[ OK ]${c_off}" # _DEBUG.
}


# main exec entry point.
time main "$@"

### ## # eof. # ## ###.
